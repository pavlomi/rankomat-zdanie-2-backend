package rankomat.xmlparser
import rankomat.xmlparser.entity.{Node, Tree}

class RankomatService(rankomatXMLParser: RankomatXMLParser) {

  def getNodes(): Seq[Node] = tree.rootNodes

  private lazy val tree = Tree.buildTree(rankomatXMLParser.parse)
}
