package rankomat.xmlparser

import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import rankomat.xmlparser.entity.Node
import spray.json._

trait JsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit lazy val personFormat: JsonFormat[Node] = lazyFormat(jsonFormat3(Node.apply))
}

class RankomatRouter(rankomatService: RankomatService) extends Directives with JsonProtocol {
  val route = path("rankomat") {
    (get & pathEndOrSingleSlash) {
      complete(rankomatService.getNodes)
    }
  }
}
