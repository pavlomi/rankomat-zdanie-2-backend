package rankomat.xmlparser.entity
import rankomat.xmlparser.dto.Mapping

case class Node(id: Int, name: String, nodes: List[Node]) { self =>
  require(name.nonEmpty)

  def addChild(mapping: Mapping): Node =
    if (mapping.getParentName.contains(name))
      self.copy(nodes = nodes :+ Node.from(mapping))
    else
      self.copy(nodes = nodes.map(_.addChild(mapping)))
}

object Node {
  def from(mapping: Mapping): Node = Node(mapping.id, mapping.name, List.empty[Node])
}
