package rankomat.xmlparser

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}
import rankomat.xmlparser.config.ParserConfig
import com.softwaremill.macwire.wire

object Boot extends App {

  // bootstrap
  implicit val system           = ActorSystem("rankomat")
  implicit val materializer     = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  lazy val config: Config       = ConfigFactory.load()
  val parserConfig              = ParserConfig(config.getString("rankomat.file-name"))

  // Dependency injection
  lazy val rankomatXMLParser = wire[RankomatXMLParser]
  lazy val rankomatService   = wire[RankomatService]
  lazy val rankomatRouter    = wire[RankomatRouter]

  Http().bindAndHandle(rankomatRouter.route, "localhost", 8080)
}
