package rankomat.xmlparser.dto

case class Mapping(id: Int, name: String) {
  require(name.nonEmpty)
  def getParentName: Option[String] = {
    val parentName = name.substring(0, name.length - 1).trim
    if (parentName.isEmpty) None else Some(parentName)
  }
}
