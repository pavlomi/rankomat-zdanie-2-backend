package rankomat.xmlparser
import org.apache.poi.xssf.usermodel.{XSSFRow, XSSFSheet, XSSFWorkbook}
import rankomat.xmlparser.config.ParserConfig
import rankomat.xmlparser.dto.Mapping

class RankomatXMLParser(config: ParserConfig) {
  lazy val parse: Seq[Mapping] = {
    lazy val wb    = new XSSFWorkbook(getClass.getResourceAsStream("/" + config.filename))
    lazy val sheet = wb.getSheetAt(0)

    for {
      rowNumber <- DATA_ROWS
      row = getRow(sheet, rowNumber)
    } yield Mapping(getId(row), getLevelName(row))
  }

  private def getLevelName(row: XSSFRow) = {
    val firstCellValue  = row.getCell(FIRST_LEVEL_NUMBER).getStringCellValue
    val secondCellValue = row.getCell(SECOND_LEVEL_NUMBER).getStringCellValue
    val thirdCellValue  = row.getCell(THIRD_LEVEL_NUMBER).getStringCellValue

    if (firstCellValue.nonEmpty) firstCellValue
    else if (secondCellValue.nonEmpty) secondCellValue
    else if (thirdCellValue.nonEmpty) thirdCellValue
    else throw RankomatXMLParser.CellParseError
  }

  private def getId(row: XSSFRow): Int = row.getCell(ID_NUMBER).getNumericCellValue.toInt

  private def getRow(sheet: XSSFSheet, i: Int) = sheet.getRow(i)

  private val DATA_ROWS           = 1 to 12
  private val FIRST_LEVEL_NUMBER  = 0
  private val SECOND_LEVEL_NUMBER = 1
  private val THIRD_LEVEL_NUMBER  = 2
  private val ID_NUMBER           = 3

}

object RankomatXMLParser {
  case object CellParseError extends Exception("Three field cannot be empty.")
}
