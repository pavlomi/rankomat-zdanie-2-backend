package rankomat.xmlparser.entity
import org.scalatest.FlatSpec
import rankomat.xmlparser.dto.Mapping

class NodeSpec extends FlatSpec {

  val random = scala.util.Random

  "addChild" should "return self with new child" in {
    val parentNode = Node(random.nextInt, "A", List.empty[Node])
    val childNode  = Node(random.nextInt, "AA", List.empty[Node])
    val mapping    = Mapping(childNode.id, childNode.name)

    assert(parentNode.addChild(mapping) == parentNode.copy(nodes = List(childNode)))
  }

  "addChild" should "return self with new subchild" in {
    val childNode    = Node(random.nextInt, "AA", List.empty[Node])
    val parentNode   = Node(random.nextInt, "A", List(childNode))
    val subChildNode = Node(random.nextInt, "AA1", List.empty[Node])
    val mapping      = Mapping(subChildNode.id, subChildNode.name)

    assert(parentNode.addChild(mapping) == parentNode.copy(nodes = List(childNode.copy(nodes = List(subChildNode)))))
  }

  "addChild" should "return self without new child" in {
    val parentNode = Node(random.nextInt, "A", List.empty[Node])
    val childNode  = Node(random.nextInt, "BB", List.empty[Node])
    val mapping    = Mapping(childNode.id, childNode.name)

    assert(parentNode.addChild(mapping) == parentNode)
  }

}
