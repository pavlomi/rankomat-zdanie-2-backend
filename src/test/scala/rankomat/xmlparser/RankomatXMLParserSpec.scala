package rankomat.xmlparser
import org.scalatest.FlatSpec
import rankomat.xmlparser.config.ParserConfig

class RankomatXMLParserSpec extends FlatSpec {

  val parserConfig = ParserConfig("for-test.xlsx")

  val rankomatXMLParser = new RankomatXMLParser(parserConfig)

  "parse" should "return list of mapping" in {
    assert(rankomatXMLParser.parse.size == 12)
  }

}
