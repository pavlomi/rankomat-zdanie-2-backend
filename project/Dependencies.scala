import sbt._

object Versions {
  val akkaV               = "2.5.15"
  val akkaHttpV           = "10.1.4"
  val macwireV            = "2.3.1"
  val slickV              = "3.2.3"
  val slickPgV            = "0.16.3"
  val postgresqlV         = "42.2.4"
  val logbackV            = "1.2.3"
  val logbackLogstashEncV = "5.2"
  val scalaTestV          = "3.0.5"
  val commonsV            = "3.8"
  val nexusUtilsV         = "1.1.0.1"
  val scalaCsv            = "1.3.5"
  val bcryptV             = "3.1"
  val kebsV               = "1.6.1"
  val gusV                = "0.0.6_2.12"
  val scrimageV           = "2.1.8"
  val catsV               = "1.2.0"
  val h2databaseV         = "1.4.197"
  val flywayV             = "5.1.4"
  val testEmbeddedKafka   = "2.0.0"
  val apacheXml           = "3.9"
}

object Dependencies {
  import Versions._

  lazy val akkaBase = Seq(
    "com.typesafe.akka" %% "akka-actor"  % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-slf4j"  % akkaV
  )

  lazy val akkaHttp = Seq(
    "com.typesafe.akka" %% "akka-http-core"       % akkaHttpV,
    "com.typesafe.akka" %% "akka-http"            % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpV % "test"
  )

  lazy val macwire = Seq(
    "com.softwaremill.macwire" %% "macros" % macwireV % "provided",
    "com.softwaremill.macwire" %% "util"   % macwireV,
    "com.softwaremill.macwire" %% "proxy"  % macwireV
  )

  lazy val logback = Seq(
    "ch.qos.logback"       % "logback-classic"          % logbackV,
    "net.logstash.logback" % "logstash-logback-encoder" % logbackLogstashEncV
  )

  lazy val test = Seq(
    "org.scalatest" %% "scalatest"                   % scalaTestV % "test",
  )

  lazy val others = Seq(
    "org.apache.commons" % "commons-lang3"    % commonsV,
    "pl.iterators"       %% "kebs-spray-json" % kebsV,
    "org.apache.poi"     % "poi"              % apacheXml,
    "org.apache.poi"     % "poi-ooxml"        % apacheXml,
    "org.typelevel"      %% "cats-core"       % catsV
  )

  lazy val dependencies = akkaBase ++ akkaHttp ++ macwire ++ logback ++ test ++ others
}
